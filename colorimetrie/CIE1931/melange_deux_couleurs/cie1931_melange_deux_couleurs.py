# python 3
# (C) Fabrice Sincère
# diagramme de chromaticité CIE 1931

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


def melange_2_couleurs(c1, c2):
    """
c1 : 1ère couleur : liste [luminance, x ,y]
c2 : 2ème couleur : liste [luminance, x ,y]
retourne le tuple (luminance, x ,y)
"""
    l1, x1, y1 = c1
    l2, x2, y2 = c2
    luminance = l1+l2
    x = (x1*l1/y1 + x2*l2/y2)/(l1/y1 + l2/y2)
    y = (l1+l2)/(l1/y1 + l2/y2)
    return luminance, x, y


def saisie_donnees():
    while True:
        try:
            c = list(map(float, input("Entrer les coefficients séparés par une \
virgule : ").split(",")))
            if len(c) == 3:
                break
        except ValueError:
            pass
    return c


print("Diagramme de chromaticité CIE 1931")
print("1ère couleur : luminance, x, y")
c1 = saisie_donnees()

print("2ème couleur : luminance, x, y")
c2 = saisie_donnees()

luminance, x, y = melange_2_couleurs(c1, c2)

print("\nRésultat du mélange des deux couleurs :")

print("""
Luminance : {}
x : {}
y : {}
""".format(luminance, x, y))


"""Exemple
Diagramme de chromaticité CIE 1931
1ère couleur : luminance, x, y
Entrer les coefficients séparés par une virgule : 5000,0.2,0.6
2ème couleur : luminance, x, y
Entrer les coefficients séparés par une virgule : 8000,0.4,0.52

Résultat du mélange des deux couleurs :

Luminance : 13000.0
x : 0.3297297297297297
y : 0.5481081081081081
"""
