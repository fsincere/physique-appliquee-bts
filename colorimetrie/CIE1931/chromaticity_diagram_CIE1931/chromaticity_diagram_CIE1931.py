# python 3

# installation du module colour
# pip install colour-science

from colour.plotting import *
fig, ax = plot_chromaticity_diagram_CIE1931(cmfs='CIE 1931 2 Degree Standard Observer',
                                            show_diagram_colours=True,
                                            show_spectral_locus=True)
