# python 3
# fabrice sincere

# installation du module colour (v0.4.4)
# pip install colour-science

import math

try:
    import colour
    from colour.colorimetry import MSDS_CMFS
except ImportError:
    print("""Il faut installer le module colour :
pip install colour-science""")
    exit(1)


__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


def distance(p1, p2):
    """Calcul la distance entre deux points dans le plan cartésien
Exemple :
>>> distance([0.62, 0.29], [1/3, 1/3])
0.28992336152086046
"""
    x1, y1 = p1
    x2, y2 = p2
    return math.sqrt((x1-x2)**2+(y1-y2)**2)


print("Diagramme de chromaticité CIE 1931")
print("Coordonnées de la couleur x, y :")

while True:
    try:
        xy = list(map(float, input("Entrer les coefficients séparés par une \
virgule : ").split(",")))
        if len(xy) == 2:
            break
    except ValueError:
        pass

print("Coordonnées de l'illuminant x, y :")

while True:
    try:
        xy_n = list(map(float, input("Entrer les coefficients séparés par une \
virgule : ").split(",")))
        if len(xy_n) == 2:
            break
    except ValueError:
        pass


longueur_onde_dominante, intersection1, intersection2 = colour.\
    dominant_wavelength(xy, xy_n,
                        cmfs=MSDS_CMFS["CIE 1931 2 Degree Standard Observer"],
                        inverse=False)
longueur_onde_complementaire, intersection1c, intersection2c = colour.\
    dominant_wavelength(xy, xy_n,
                        cmfs=MSDS_CMFS["CIE 1931 2 Degree Standard Observer"],
                        inverse=True)

if longueur_onde_dominante > 0:
    # couleur dominante sur le spectrum locus
    print("Longueur d'onde dominante : {} nm".format(longueur_onde_dominante))
    print("Intersection S avec le spectrum locus : ({}, {})"
          .format(*intersection1))
    distanceWC = distance(xy, xy_n)
    distanceWS = distance(intersection1, xy_n)
    print("Saturation = WC/WS : {} %".format(distanceWC*100/distanceWS))
    colorimetricpurity = colour.\
        colorimetric_purity(xy, xy_n,
                            cmfs=MSDS_CMFS['CIE 1931 2 Degree Standard \
Observer'])
    print("Colorimetric purity : {} %".format(colorimetricpurity*100))

    # couleur complémentaire
    if longueur_onde_complementaire > 0:
        # couleur complémentaire sur le spectrum locus
        print("Longueur d'onde complémentaire : {} nm"
              .format(longueur_onde_complementaire))
        print("Intersection S' avec le spectrum locus : ({}, {})"
              .format(*intersection1c))
    else:
        # couleur complémentaire sur la droite des pourpres
        print("Intersection avec la droite des pourpres : pas de longueur \
d'onde complémentaire")
        print("Intersection S' avec la droite des pourpres : ({}, {})"
              .format(*intersection1c))

else:
    # couleur dominante sur la droite des pourpres
    print("Intersection avec la droite des pourpres : pas de longueur d'onde \
dominante")
    print("Intersection S avec la droite des pourpres : ({}, {})"
          .format(*intersection1))

    # couleur complémentaire
    if longueur_onde_complementaire > 0:
        # couleur complémentaire sur le spectrum locus
        print("Longueur d'onde complémentaire : {} nm"
              .format(longueur_onde_complementaire))
        print("Intersection S' avec le spectrum locus : ({}, {})"
              .format(*intersection1c))
    else:
        # couleur complémentaire sur la droite des pourpres
        # ça ne se peut pas !
        pass

""" 3 cas typiques :
Exemple1
========
Diagramme de chromaticité CIE 1931
Coordonnées de la couleur x, y :
Entrer les coefficients séparés par une virgule : 0.62,0.29
Coordonnées de l'illuminant x, y :
Entrer les coefficients séparés par une virgule : 0.333,0.333

Longueur d'onde dominante : 650.0 nm
Intersection S avec le spectrum locus : (0.725859692242034, 0.2741394886187893)
Saturation = WC/WS : 73.05407138159246 %
Colorimetric purity : 69.05864058644927 %
Longueur d'onde complémentaire : 494.0 nm
Intersection S' avec le spectrum locus :
(0.029140556565408704, 0.37852597932992144)

Exemple2
========
Diagramme de chromaticité CIE 1931
Coordonnées de la couleur x, y :
Entrer les coefficients séparés par une virgule : 0.1,0.7
Coordonnées de l'illuminant x, y :
Entrer les coefficients séparés par une virgule : 0.333,0.333

Longueur d'onde dominante : 514.0 nm
Intersection S avec le spectrum locus : (0.0337578079078652, 0.8043385600764525)
Saturation = WC/WS : 77.86335154511258 %
Colorimetric purity : 89.46928009217497 %
Intersection avec la droite des pourpres : pas de longueur d'onde complémentaire
Intersection S' avec la droite des pourpres :
(0.4577415432290226, 0.13651868512853535)

Exemple3
========
Diagramme de chromaticité CIE 1931
Coordonnées de la couleur x, y :
Entrer les coefficients séparés par une virgule : 0.4,0.2
Coordonnées de l'illuminant x, y :
Entrer les coefficients séparés par une virgule : 0.333,0.333

Intersection avec la droite des pourpres : pas de longueur d'onde dominante
Intersection S avec la droite des pourpres :
(0.43686904512304725, 0.12681219400947352)
Longueur d'onde complémentaire : 521.0 nm
Intersection S' avec le spectrum locus :
(0.0805982092804714, 0.8340363905327957)
"""
