# python 3
# fabrice sincere

# installation du module colour
# pip install colour-science

try:
    import colour
except ImportError:
    print("""Il faut installer le module colour :
pip install colour-science""")
    exit(1)


__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


print("Saisie des valeurs Teinte, Saturation, Valeur")
print("0 à 360° ; 0 à 100 % ; 0 à 100 %")
print("CTRL+C pour quitter")

while True:
    try:
        while True:
            try:
                # hue, saturation, value
                res = tuple(map(float, input("Entrer les coefficients séparés par une virgule : ").split(",")))
                if len(res) == 3:
                    teinte, saturation, valeur = res
                    if 0 <= teinte <= 360 and 0 <= saturation <= 100 and 0 <= valeur <= 100:
                        teinte /= 360  # on ramène à l'intervalle [0, 1]
                        saturation /= 100
                        valeur /= 100
                        break
            except ValueError:
                pass

        rouge, vert, bleu = colour.HSV_to_RGB([teinte, saturation, valeur])
        rouge *= 255  # on ramène à l'intervalle [0, 255] (comme dans GIMP)
        vert *= 255
        bleu *= 255
        print("RVB : {} {} {}".format(rouge, vert, bleu))
    except KeyboardInterrupt:
        print("\nbye")
        break

"""Exemple :
Saisie des valeurs Teinte, Saturation, Valeur
0 à 360° ; 0 à 100 % ; 0 à 100 %
CTRL+C pour quitter
Entrer les coefficients séparés par une virgule : 25,80,70
RVB : 178.5 95.20000000000002 35.69999999999999
Entrer les coefficients séparés par une virgule :
"""
