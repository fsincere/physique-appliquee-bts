# python 3
# fabrice sincere

# installation du module colour
# pip install colour-science

try:
    import colour
except ImportError:
    print("""Il faut installer le module colour :
pip install colour-science""")
    exit(1)

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"

print("Saisie des valeurs Rouge, Vert, Bleu")
print("(valeurs entre 0 et 255)")
print("CTRL+C pour quitter")

while True:
    try:
        while True:
            try:
                res = tuple(map(float, input("Entrer les coefficients séparés par une virgule : ").split(",")))
                if len(res) == 3:
                    rouge, vert, bleu = res
                    if 0 <= vert <= 255 and 0 <= bleu <= 255 and 0 <= rouge <= 255:
                        vert /= 255  # on ramène à l'intervalle [0, 1]
                        bleu /= 255
                        rouge /= 255
                        break
            except ValueError:
                pass

        # hue, saturation, value
        teinte, saturation, valeur = colour.RGB_to_HSV([rouge, vert, bleu])
        teinte *= 360  # on ramène à l'intervalle [0, 360°] (comme dans GIMP)
        saturation *= 100  # on ramène à l'intervalle [0, 100 %] (comme dans GIMP)
        valeur *= 100  # on ramène à l'intervalle [0, 100 %] (comme dans GIMP)
        print("HSV : {}° {} % {} %".format(teinte, saturation, valeur))
    except KeyboardInterrupt:
        print("\nbye")
        break

"""Exemple :
Saisie des valeurs Rouge, Vert, Bleu
(valeurs entre 0 et 255)
CTRL+C pour quitter
Entrer les coefficients séparés par une virgule : 178.5,95.2,35.7
HSV : 24.999999999999993° 80.0 % 70.0 %
Entrer les coefficients séparés par une virgule :
"""
