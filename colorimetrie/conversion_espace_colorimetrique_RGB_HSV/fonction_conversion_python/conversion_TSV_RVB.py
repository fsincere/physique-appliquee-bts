# python 3 ; MicroPython
# (C) Fabrice Sincère

__version__ = (0, 0, 1)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


def TSV_vers_RVB(teinte, saturation, valeur):
    """Conversion de l'espace colorimétrique TSV (HSV) vers RVB
teinte (hue) : 0 à 360 (degrés)
saturation : 0 à 100 (%)
valeur (value) : 0 à 100 (%)

retourne :
rouge, vert, bleu
(0 à 255)
"""
    teinte %= 360  # on ramène le teinte entre 0 et 360°
    if not 0 <= saturation <= 100:
        raise ValueError("La saturation doit être comprise entre 0 et 100 %")

    if not 0 <= valeur <= 100:
        raise ValueError("La valeur doit être comprise entre 0 et 100 %")

    def fonction_de_repartition(teinte):
        teinte %= 360
        if teinte <= 60 or teinte >= 300:
            return 255
        elif 120 <= teinte <= 240:
            return 0
        elif 60 <= teinte <= 120:
            return 255-(teinte-60)*255/60
        elif 240 <= teinte <= 300:
            return (teinte-240)*255/60

    # saturation et valeur à 100 %
    rouge = fonction_de_repartition(teinte)
    vert = fonction_de_repartition(teinte-120)
    bleu = fonction_de_repartition(teinte+120)

    # saturation (pureté)
    rouge = 255 - (255-rouge)*saturation/100
    vert = 255 - (255-vert)*saturation/100
    bleu = 255 - (255-bleu)*saturation/100

    # valeur (intensité)
    rouge *= valeur/100
    vert *= valeur/100
    bleu *= valeur/100

    return rouge, vert, bleu


print("Saisie des valeurs Teinte, Saturation, Valeur")
print("0 à 360° ; 0 à 100 % ; 0 à 100 %")
print("CTRL+C pour quitter")

while True:
    try:
        while True:
            try:
                # hue, saturation, value
                res = tuple(map(float, input("Entrer les coefficients séparés \
par une virgule : ").split(",")))
                if len(res) == 3:
                    teinte, saturation, valeur = res
                    break
            except ValueError:
                pass

        rouge, vert, bleu = TSV_vers_RVB(teinte, saturation, valeur)
        print("RVB : {} {} {}".format(rouge, vert, bleu))
    except KeyboardInterrupt:
        break
print("\nbye")


"""Exemple :
Saisie des valeurs Teinte, Saturation, Valeur
0 à 360° ; 0 à 100 % ; 0 à 100 %
CTRL+C pour quitter
Entrer les coefficients séparés par une virgule : 25,80,70
RVB : 178.5 95.19999999999999 35.699999999999996
Entrer les coefficients séparés par une virgule :
"""
