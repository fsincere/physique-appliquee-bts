# python 3

import math

__version__ = (0, 0, 4)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


def unite_puissance(valeur):
    unite = input("Unité : dBm (Enter) ou mW ? ").lower()
    if unite != '':
        mw = valeur
        dbm = 10*math.log10(mw)  # en dBm
    else:
        dbm = valeur
        mw = 10**(dbm/10)
    return dbm, mw


def unite_gain_antenne(valeur):
    unite = input("Unité : dBi ou sans unité (Enter) ? ").lower()
    if unite != '':
        dbi = valeur
        amplification = 10**(dbi/10)  # sans unité
    else:
        amplification = valeur
        dbi = 10*math.log10(amplification)
    return dbi, amplification


print("""
#######################################################
# Antennes : bilan de liaison                         #
#######################################################
""")

pe = float(input("Puissance de l'antenne d'émission ? "))
pe_dbm, pe_mw = unite_puissance(pe)
print("{:.6g} dBm ou {:.6g} mW".format(pe_dbm, pe_mw))

ge = float(input("Gain de l'antenne d'émission ? "))
ge_dbi, ge_amplification = unite_gain_antenne(ge)
print("{:.6g} dBi ou {:.6g}".format(ge_dbi, ge_amplification))

pire_dbm = pe_dbm+ge_dbi  # en dBm
pire_mw = 10**(pire_dbm/10)  # en mW

d = 1e3*float(input("Distance (en km) ? "))  # en m
f = 1e6*float(input("Fréquence de la porteuse (en MHz) ? "))  # en Hz
longueur_onde = 2.997e8/f  # dans l'air (en m)
# longueur_onde = 299792458/f  # dans le vide (en m)

print("Longueur d'onde : {:.6g} m".format(longueur_onde))

gr = float(input("Gain de l'antenne de réception ? "))
gr_dbi, gr_amplification = unite_gain_antenne(gr)
print("{:.6g} dBi ou {:.6g}".format(gr_dbi, gr_amplification))

print("\nPIRE de l'antenne d'émission : {:.6g} dBm".format(pire_dbm))
print("PIRE de l'antenne d'émission : {:.6g} mW".format(pire_mw))

# perte de puissance en espace libre (Free Space Loss)
fsl_db = -20*math.log10(longueur_onde/(4*math.pi*d))  # en dB
print("Perte de puissance en espace libre : {:.6g} dB".format(fsl_db))

# puissance reçue
pr_dbm = pe_dbm + ge_dbi - fsl_db + gr_dbi  # en dBm
pr_mw = 10**(pr_dbm/10)
print("Puissance reçue par l'antenne de réception : {:.6g} dBm".format(pr_dbm))
print("Puissance reçue par l'antenne de réception : {:.6g} mW".format(pr_mw))

# densité de puissance reçue
densite_puissance = pire_mw*1e-3/(4*math.pi*d**2)  # en W/m2
print("Densité de puissance au niveau de l'antenne de réception : {:.6g} W/m2"
      .format(densite_puissance))

# surface effective de l'antenne de réception
surface_efficace = gr_amplification*longueur_onde**2/(4*math.pi)
print("Surface effective de l'antenne de réception : {:.6g} m2"
      .format(surface_efficace))

# champ électrique au niveau de l'antenne de réception
E = math.sqrt(30*pire_mw*1e-3)/d  # en V/m
print("Champ électrique au niveau de l'antenne de réception : {:.6g} V/m"
      .format(E))

# impédance de l'antenne de réception
Za = 75  # en ohms (Ω)
# tension induite dans l'antenne de réception
Vr = math.sqrt(pr_mw*1e-3*Za)  # en volts
print("Tension induite dans l'antenne de réception ({:.6g} Ω) : {:.6g} V"
      .format(Za, Vr))

input("Enter pour quitter")

""" Exemple d'utilisation :

Puissance de l'antenne d'émission ? 5
Unité : dBm (Enter) ou mW ?
5 dBm ou 3.16228 mW
Gain de l'antenne d'émission ? 7
Unité : dBi ou sans unité (Enter) ? dbi
7 dBi ou 5.01187
Distance (en km) ? 0.2
Fréquence de la porteuse (en MHz) ? 433.92
Longueur d'onde : 0.69068 m
Gain de l'antenne de réception ? 7
Unité : dBi ou sans unité (Enter) ? dbi
7 dBi ou 5.01187

PIRE de l'antenne d'émission : 12 dBm
PIRE de l'antenne d'émission : 15.8489 mW
Perte de puissance en espace libre : 71.2193 dB
Puissance reçue par l'antenne de réception : -52.2193 dBm
Puissance reçue par l'antenne de réception : 5.99894e-06 mW
Densité de puissance au niveau de l'antenne de réception : 3.15304e-08 W/m2
Surface effective de l'antenne de réception : 0.190259 m2
Champ électrique au niveau de l'antenne de réception : 0.00344771 V/m
Tension induite dans l'antenne de réception (75 Ω) : 0.000670761 V
"""
