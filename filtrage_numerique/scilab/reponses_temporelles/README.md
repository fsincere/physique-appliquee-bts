## Etude d'un filtre numérique avec Scilab

Données : coefficients de l'équation de récurrence

```
a0*y(n) = b0*x(n) + b1*x(n-1) + b2*x(n-2) + ...
          -a1*y(n-1) -a2*y(n-2) -a3*y(n-3) +...

b = [b0 b1 b2 ...]
a = [a0 a1 a2 ...]
```

### Fonctionnalités :

- réponse impulsionnelle
- réponse indicielle
- réponse à une rampe
- réponse à un sinus
- réponse personnalisée
