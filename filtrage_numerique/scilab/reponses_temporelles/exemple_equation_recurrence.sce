// scilab 6.1
// (C) Fabrice Sincère
// version 0.0.4

/*
Filtre numérique
Equation de récurrence (forme générale)

a0*y(n) = b0*x(n) + b1*x(n-1) + b2*x(n-2) + ...
          -a1*y(n-1) -a2*y(n-2) -a3*y(n-3) +...

(avec a0 non nul et au moins un coefficient b non nul)

b = [b0 b1 b2 ...]
a = [a0 a1 a2 ...]
*/

clear

/*
// moyenne glissante 4 échantillons (récursif)
b = [0.25 0 0 0 -0.25]
a = [1 -1]
*/

// bandstop iir elliptic
b = [0.05311849370698291,-9.389778854451691e-08,0.1742833053320179,-2.4647880066119843e-07,0.24618801160644613,-2.464788006750762e-07,0.1742833053320179,-9.389778854451691e-08,0.0531184937069829]
a = [1.0,-7.190577413496158e-07,-1.0382602127247047,2.0203282580233406e-07,1.1413120092942255,-1.64851127248955e-07,-0.5345385874651425,-1.277005795552455e-08,0.14678435179811977]

function [n, x, y] = tracer_reponse_impulsionnelle(k, ndebut, nfin)
    // réponse impulsionnelle
    Nmin=min(ndebut, 0)  // <=0
    Nmax=nfin
    n=Nmin:Nmax
    x=zeros(1,length(n)) // vecteurs de 0
    x(-Nmin+1)=1 //x pour l'indice 0 vaut 1
    x = k*x
    // fonction de transfert en z (puissance de z^-1)
    y = filter(b, a, x)
    scf()
    plot(n, x,'xb')
    plot(n, y,'or')
    xlabel('n')
    title("Réponse impulsionnelle")
    legend(['entrée x(n)', 'sortie y(n)'])   
endfunction

function [n, x, y] = tracer_reponse_indicielle(k, ndebut, nfin)
    // réponse indicielle
    Nmin=min(ndebut, 0)  // <=0
    Nmax=nfin
    n=Nmin:Nmax
    x=ones(1,length(n)) // vecteurs de 1
    x(1:-Nmin)=0 //x pour l'indice <0 vaut 0
    x = k*x
    y = filter(b, a, x)
    scf()
    plot(n, x,'xb')
    plot(n, y,'or')
    xlabel('n')
    title("Réponse indicielle")
    legend(['entrée x(n)', 'sortie y(n)'])
endfunction

function [n, x, y] = tracer_reponse_rampe(k, ndebut, nfin)
    // réponse à une rampe
    Nmin=min(ndebut, 0)  // <=0
    Nmax=nfin
    n=Nmin:Nmax
    x=zeros(1,-Nmin+1) // vecteurs de 0
    x=[x,1:Nmax] // concaténation de deux vecteurs
    x = k*x
    y = filter(b, a, x)
    scf()
    plot(n, x,'xb')
    plot(n, y,'or')
    xlabel('n')
    title("Réponse à une rampe")
    legend(['entrée x(n)', 'sortie y(n)'])   
endfunction

function [n, x, y] = tracer_reponse_sinus(f, fe, k, ndebut, nfin)
    // réponse à un sinus de fréquence f
    Nmin=min(ndebut, 0)  // <=0
    Nmax=nfin
    n=Nmin:Nmax
    x=zeros(1,-Nmin) // vecteurs de 0
    x=[x,sin(2*%pi*f/fe*(0:Nmax))]  // concaténation de deux vecteurs
    x = k*x
    y = filter(b, a, x)
    scf()
    plot(n, x,'xb')
    plot(n, y,'or')
    xlabel('n')
    title(msprintf("Réponse à un sinus %g Hz (fe = %g Hz)", f, fe))
    legend(['entrée x(n)', 'sortie y(n)'])   
endfunction

function [n, x, y] = tracer_reponse_personnalisee(x, ndebut)
    // réponse personnalisée
    Nmin=min(ndebut, 0)  // <=0
    Nmax=length(x)-1
    n=Nmin:Nmax
    z=zeros(1,-Nmin) // vecteurs de 0
    x=[z, x] // concaténation de deux vecteurs
    y = filter(b, a, x)
    scf()
    plot(n, x,'xb')
    plot(n, y,'or')
    xlabel('n')
    title("Réponse personnalisée")
    legend(['entrée x(n)', 'sortie y(n)'])   
endfunction


[n, x, y] = tracer_reponse_impulsionnelle(k=1, ndebut=-2, nfin=50)
mprintf("\nRéponse impulsionnelle")
disp([n' x' y'])

[n, x, y] = tracer_reponse_indicielle(k=1, ndebut=-2, nfin=50)
mprintf("\nRéponse indicielle")
disp([n' x' y'])

[n, x, y] = tracer_reponse_rampe(k=1, ndebut=-2, nfin=50)
mprintf("\nRéponse à une rampe")
disp([n' x' y'])

f=1000 // fréquence du sinus en Hz
fe=20000 // fréquence d'échantillonnage en Hz

[n, x, y] = tracer_reponse_sinus(f=1000, fe=20000, k=1, ndebut=-3, nfin=30)
mprintf("\nRéponse à un sinus %g Hz (fe = %g Hz)", f, fe)
disp([n' x' y'])

x = [1 1 1 1 1 0 0 0 0 0]  // n >= 0

[n, x, y] = tracer_reponse_personnalisee(x=x, ndebut=-3)
mprintf("\nRéponse personnalisée")
disp([n' x' y'])
