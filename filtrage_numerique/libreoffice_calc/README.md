## Etude d'un filtre numérique avec un tableur LibreOffice

Données : coefficients de l'équation de récurrence

a0*y(n) = b0*x(n) + b1*x(n-1) + b2*x(n-2) + ...
          -a1*y(n-1) -a2*y(n-2) -a3*y(n-3) +...
          
ordre 4 max

Fonctionnalités :

- réponse impulsionnelle
- réponse indicielle
- réponse à une rampe
- réponse à un sinus

- réponse en fréquence (gain/phase)
