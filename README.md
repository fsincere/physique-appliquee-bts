## Outils pédagogiques pour la Physique appliquée en BTS


- Antennes : bilan de liaison d'une communication radiofréquence
    + Equation de Friis

- Colorimétrie
    + Espace colorimétrique CIE 1931
    + Espaces colorimétriques RGB et HSV

- Filtrage numérique
    + avec un tableur
    + avec Scilab

- Oscilloscopes Agilent/Keysight InfiniiVision DSO séries 1000
    + acquisition de signaux
    + décodage de protocoles avec PulseView/Sigrok

