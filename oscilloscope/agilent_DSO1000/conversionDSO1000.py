# python3

# test : Agilent DSO1004A Python38/Pulseview050/Sigrok080 sous Linux Ubuntu OK
# test : Agilent DSO1004A Python38/Pulseview050/Sigrok080 sous windows 10   OK

import os
import csv
try:
    import matplotlib.pyplot as plt  # optionnel
except ImportError:
    matplotlib_enabled = False
    print('matplotlib absent')
else:
    matplotlib_enabled = True

__version__ = (0, 0, 8)
__author__ = "Fabrice Sincère <fabrice.sincere@ac-grenoble.fr>"


print("""Oscilloscope Agilent/Keysight InfiniiVision DSO séries 1000
-----------------------------------------------------------

Ce script permet de convertir le fichier de données CSV
issu de l'oscilloscope (Save -> CSV sur clé usb externe)
pour une importation dans le logiciel PulseView (Open -> import CSV)
ou Sigrok (en ligne de commande)

L'objectif est de profiter des fonctions de décodage de PulseView/Sigrok
(UART, I2C, IR NEC...)
""")

# fichier dans le répertoire courant
while True:
    filename = input('Nom du fichier d\'entrée ? ')
    if os.path.isfile(filename) is False:
        print("File does not exist")
    else:
        break

# structure du fichier de sauvegarde csv de l'oscilloscope :
'''
,CH1,CH2,CH3
Second,Volt,Volt,Volt
-6.00000e-02,1.20e-02,4.00e-04,2.00e-04,
-5.98000e-02,2.60e-02,2.00e-04,4.00e-04,
-5.96000e-02,1.60e-02,2.00e-04,4.00e-04,
...
'''

# il y a certainement plus élégant avec numpy...

time = list()  # secondes
voltage_channels = list()  # analog  volts
voltage_channels_binary = list()  # binary 0/1
channel_name = list()
seuils = list()  # # threshold

try:
    with open(filename, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        for line, row in enumerate(spamreader):
            if line == 0:
                # 1ère ligne : header
                # ,CH1,CH2,CH3
                channels_number = len(row)-1
                if channels_number < 1:
                    raise ValueError('line 1 header incorrect')
                for channel in range(channels_number):
                    voltage_channels.append([])
                    voltage_channels_binary.append([])
                    # ['CH1', 'CH2', 'CH3']
                    channel_name.append(str(row[channel+1]))
            elif line == 1:
                # Second,Volt,Volt,Volt
                if len(row) != channels_number+1 or row[0] != 'Second':
                    raise ValueError('line 2 units incorrect')
            elif line > 1:
                # datas à partir de la 3ème ligne
                # -6.00000e-02,1.20e-02,4.00e-04,2.00e-04,
                if len(row) > 0:
                    # on ignore les lignes vides
                    if len(row) != channels_number+2:
                        raise ValueError('line {} incorrect'.format(line+1))
                    for i, val in enumerate(row[:-1]):
                        if i == 0:
                            time.append(float(val))
                        else:
                            voltage_channels[i-1].append(float(val))
except Exception as e:
    print(e)
    print("Format de fichier invalide")
    exit(1)


# nombre d'échantillons
N = len(time)
print("\nNombre d'échantillons :", N)

if N < 2:
    print("Erreur : nombre d'échantillons insuffisant")
    exit(2)

# Number of analog channels
print('Nombre de voies :', channels_number)

Te = (time[-1]-time[0])/(len(time)-1)
print("Période d'échantillonnage   : {:.6g} s".format(Te))
# sampling rate
fe = 1/Te
print("Fréquence d'échantillonnage : {:.6g} Hz".format(fe))

if matplotlib_enabled:
    # plot matplotlib

    fig, ax = plt.subplots()
    ax.grid(True, which="both")
    ax.set_title("Analog", fontsize=14)
    ax.set_xlabel("Temps (s)")
    ax.set_ylabel("Tension (V)")

    for i in range(channels_number):
        ax.plot(time, voltage_channels[i], linewidth=2.0, label=channel_name[i])

    plt.legend()
    fig.tight_layout()

# threshold
# seuillage

for channel in range(channels_number):
    print('\nVoie :', channel_name[channel])
    print('Valeur max : {:+.6g}'.format(max(voltage_channels[channel])))
    print('Valeur min : {:+.6g}'.format(min(voltage_channels[channel])))
    # average voltage
    average_voltage = sum(voltage_channels[channel])/len(voltage_channels[channel])
    print('Valeur moyenne : {:+.6g}'.format(average_voltage))

    try:
        seuil = float(input('Seuil de binarisation ? '))
    except Exception as e:
        # print(e)
        # on impose la valeur moyenne en cas de problème
        seuils.append(float('{:.6f}'.format(average_voltage)))
        print('Valeur retenue :', average_voltage)
    else:
        seuils.append(seuil)

    for val in voltage_channels[channel]:
        voltage_channels_binary[channel].append(1 if val > seuils[channel] else 0)

if matplotlib_enabled:
    # plot matplotlib

    for i in range(channels_number):
        fig, ax = plt.subplots()
        ax.grid(True, which="both")
        ax.set_title(channel_name[i], fontsize=14)
        ax.set_xlabel("Temps (s)")
        ax.set_ylabel("Niveau")
        ax.plot(time, voltage_channels[i], label='Analog (V)')
        ax.axhline(y=seuils[i], linestyle='--', label='Threshold (V)')
        ax.step(time, voltage_channels_binary[i], label='Binary')
        plt.legend()
        fig.tight_layout()


# création du fichier csv pour importation dans Pulseview/Sigrok

# Structure du fichier
# avec commentaires (; par défaut dans sigrok)
# et header (1ère ligne)

'''
;sampling rate 100000
;threshold,1.65,1.5,2.0
CH1,CH2,CH3
0,1,0
1,0,0
0,1,0
1,1,1
1,0,1
1,0,0
...
'''

# fichier de sortie
filename_result = 'result_'+filename
with open(filename_result, 'w', newline='') as csvfile:
    spamwriter = csv.writer(csvfile, delimiter=',')
    # comment sampling rate Hz
    spamwriter.writerow([";sampling rate {:.6g}".format(fe)])
    # comment thresholds
    spamwriter.writerow([";threshold"]+seuils)
    # header
    spamwriter.writerow(channel_name)
    for i in range(N):
        spamwriter.writerow([voltage_channels_binary[j][i] for j in range(channels_number)])

print('\nCréation du fichier {} [DONE]'.format(filename_result))

if matplotlib_enabled:
    plt.show()
