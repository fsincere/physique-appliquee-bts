## Oscilloscopes Agilent/Keysight InfiniiVision DSO séries 1000

C'est une série d'oscilloscopes commercialisée depuis 2008.  
Elle ne possède pas de fonction de décodage de protocoles.

![scope_agilent1000](https://framagit.org/fsincere/physique-appliquee-bts/-/raw/main/oscilloscope/agilent_DSO1000/images/scope.jpg)

### Objectif

Il s'agit de convertir le fichier de données CSV issu de l'oscilloscope (Save -> CSV sur clé usb externe) pour une importation dans le logiciel PulseView (Open ->  import CSV) ou Sigrok (en ligne de commande).   

L'objectif est de profiter des fonctions de décodage de PulseView/Sigrok (UART, I2C, IR NEC...).  

### 1. PulseView/Sigrok

Sigrok est une suite logicielle d’analyse logique et de décodage de protocole.

C'est un logiciel libre disponible notamment pour Linux et Windows :  

[https://sigrok.org/](https://sigrok.org/)

PulseView est l'interface graphique de Sigrok pour les analyseurs logiques et les oscilloscopes numériques.

### 2. Acquisition et sauvegarde des signaux depuis l'oscilloscope

Exemple avec un signal RS232 (UART) :

![scope_screen_uart](https://framagit.org/fsincere/physique-appliquee-bts/-/raw/main/oscilloscope/agilent_DSO1000/images/scope_uart.png)

Save -> CSV -> Taille mémoire : Affiché ou Maximum

#### Taille mémoire : Affiché

600 points

Ici avec une base de temps de 500 µs/div :  

Durée d'échantillonnage : 12 divisions &times; 500 µs = 6 ms  
Période d'échantillonnage :   Te = 6 ms/600 = 10 µs  
Fréquence d'échantillonnage : fe = 1/Te = 100 kHz  

#### Taille mémoire : Maximum

10240 points

Par rapport à la taille mémoire Affiché, la fréquence d'échantillonnage est 5 fois plus rapide.

Période d'échantillonnage :   Te = 10 µs/5 = 2 µs  
Fréquence d'échantillonnage : fe = 500 kHz  
Durée d'échantillonnage :  3.41333 fois la largeur affichée (10240/600/5)

#### Sauvegarde sur clé USB externe

Save -> Externe -> Nouveau fichier -> Filename : NewFile0.csv -> Sauvegarder


### 3. Le script Python conversionDSO1000.py

Dans la console Python :  

```
Nom du fichier d'entrée ? NewFile0.csv
Nombre d'échantillons : 600
Nombre de voies : 1
Période d'échantillonnage   : 1e-05 s
Fréquence d'échantillonnage : 100000 Hz

Voie : CH1
Valeur max : +3.6
Valeur min : -0.24
Valeur moyenne : +2.44093
Seuil de binarisation ? 1.65

Création du fichier result_NewFile0.csv [DONE]
```

![python_figure_uart](https://framagit.org/fsincere/physique-appliquee-bts/-/raw/main/oscilloscope/agilent_DSO1000/images/python_uart.png)

### 4. Importation dans PulseView

Open -> import CSV : result_NewFile0.csv

Dans les paramètres d'importation, il faut préciser la fréquence d'échantillonnage (affichée par le script Python ou disponible comme commentaire en éditant le fichier result_NewFile0.csv) :

![PulseView_v050_import_csv](https://framagit.org/fsincere/physique-appliquee-bts/-/raw/main/oscilloscope/agilent_DSO1000/images/pulseview050_import_csv.png)

Add protocol decoder : UART (baudrate : 9600) 

![PulseView_uart_decoder](https://framagit.org/fsincere/physique-appliquee-bts/-/raw/main/oscilloscope/agilent_DSO1000/images/pulseview_uart.png)

#### La même chose en ligne de commande avec Sigrok :  

```
$ sigrok-cli -I csv:header=yes:samplerate=100000 -i result_NewFile0.csv
META samplerate: 100000
libsigrok 0.6.0
Acquisition with 1/1 channels at 100 kHz
CH1:11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111
CH1:11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111
CH1:11111111 10000000 00000000 00000011 11111111 10000000 00000000 00000111
CH1:11111111 11111111 11111111 11111111 11111111 11111111 10000000 00000000
CH1:00000000 00000000 00000000 00011111 11111000 00000001 11111111 11100000
CH1:00000111 11111111 11111111 10000000 00001111 11111111 11111111 10000000
CH1:00011111 11111000 00000000 00000000 00000000 00000000 00000001 11111111
CH1:11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111
CH1:11111111 11111111 11111111 11111111 11111111 11111111 11111111 11111111
CH1:11111111 11111111 11111111
```

```
$ sigrok-cli -I csv:header=yes:samplerate=100000 -i result_NewFile0.csv -P uart:baudrate=9600
uart-1: Start bit
uart-1: 0
uart-1: 1
uart-1: 0
uart-1: 0
uart-1: 1
uart-1: 1
uart-1: 1
uart-1: 1
uart-1: F2
uart-1: Stop bit
uart-1: Start bit
uart-1: 0
uart-1: 0
uart-1: 0
uart-1: 1
uart-1: 0
uart-1: 1
uart-1: 0
uart-1: 1
uart-1: A8
uart-1: Stop bit
uart-1: Start bit
uart-1: 1
uart-1: 1
uart-1: 0
uart-1: 1
uart-1: 0
uart-1: 0
uart-1: 0
uart-1: 0
uart-1: 0B
uart-1: Stop bit
```

### 5. Exemple avec le bus i2c

Avec un capteur de température DS1631.

SDA : CH1  
SCL : CH2  

![scope_screen_i2c](https://framagit.org/fsincere/physique-appliquee-bts/-/raw/main/oscilloscope/agilent_DSO1000/images/scope_i2c.png)

Taille mémoire : Maximum  
Fréquence d'échantillonnage : 5 MHz  

PulseView : Add protocol decoder : I2C  

![pulseview_i2c_decoder](https://framagit.org/fsincere/physique-appliquee-bts/-/raw/main/oscilloscope/agilent_DSO1000/images/pulseview_i2c.png)

Adresse i2c : 0x4F  
Data : 0x1AC0 -> 26.75 °C  

#### En ligne de commande avec Sigrok :  

```
$ sigrok-cli -I csv:header=yes:samplerate=5000000 -i result_NewFile0.csv -P i2c:scl=CH2:sda=CH1
i2c-1: Start
i2c-1: 01111001
i2c-1: Write
i2c-1: Address write: 4F
i2c-1: ACK
i2c-1: 01010101
i2c-1: Data write: AA
i2c-1: ACK
i2c-1: Start repeat
i2c-1: 11111001
i2c-1: Read
i2c-1: Address read: 4F
i2c-1: ACK
i2c-1: 01011000
i2c-1: Data read: 1A
i2c-1: ACK
i2c-1: 00000011
i2c-1: Data read: C0
i2c-1: NACK
i2c-1: Stop
```

### 6. Exemple avec le protocole NEC (télécommande infrarouge)

En réception, avec signal démodulé (TSOP1238) :  

![scope_screen_nec](https://framagit.org/fsincere/physique-appliquee-bts/-/raw/main/oscilloscope/agilent_DSO1000/images/scope_nec.png)

Taille mémoire : Maximum  
Fréquence d'échantillonnage : 25000 Hz  

PulseView : Add protocol decoder : IR NEC  

![PulseView_nec_decoder](https://framagit.org/fsincere/physique-appliquee-bts/-/raw/main/oscilloscope/agilent_DSO1000/images/pulseview_nec.png)


#### En ligne de commande avec Sigrok :  

```
$ sigrok-cli -I csv:header=yes:samplerate=25000 -i result_NewFile1.csv -P ir_nec
ir_nec-1: AGC pulse
ir_nec-1: Long pause
ir_nec-1: Leader code
ir_nec-1: 00000000
ir_nec-1: Address: 0x00
ir_nec-1: 11111111
ir_nec-1: Address#: 0xFF
ir_nec-1: 10100010
ir_nec-1: Command: 0x45
ir_nec-1: 01011101
ir_nec-1: Command#: 0xBA
ir_nec-1: Stop bit
```

### (7. Interface directe entre l'oscilloscope et PulseView/Sigrok)

Sans passer par une clé USB intermédiaire...

On branche un câble USB entre l'ordinateur et le connecteur "USB device" en face arrière de l'oscilloscope.

On utilise les drivers rigol-ds

```
$ sigrok-cli -d rigol-ds --scan
The following devices were found:
rigol-ds - Agilent DSO1004A 00.04.06 SP05 with 4 channels: CH1 CH2 CH3 CH4

$ sigrok-cli -d rigol-ds --show
```

L'oscilloscope passe alors en mode remote (clavier verrouillé).  

La même chose avec l'interface graphique de PulseView :  

![PulseView_rigol_ds](https://framagit.org/fsincere/physique-appliquee-bts/-/raw/main/oscilloscope/agilent_DSO1000/images/pulseview_rigol_ds.png)

Malheureusement, cela bugue de manière aléatoire dans la phase d'acquisition (testé avec PulseView 0.5.0 / Sigrok 0.8.0).  
Pour l'instant, je continue à passer par une clé USB.


&copy; Fabrice Sincère